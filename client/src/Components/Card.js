import React from 'react';
import img1 from './Images/img1.JPEG'
const Card = props => {
    return(
        <div className='card-text-center' style={{width: "20rem", border: "solid #AE3034", borderRadius: '10px', margin: "40px", height: "550px",  backgroundColor:"#ffffff"  }} >
          <div style={{margin: "25px"   }}>
            <img src={img1} alt='seerem' />
          </div>
          <div className='card-body text-dark' style={{ height: "550px", margin: "25px"   }} >
              <h3 className='card-title' style={{color: "#AE3034"}}>Name:<br/><h4 style={{color: "#000000"}} >Jose Heredia</h4></h3><br></br>
              <h3 className='card-text'style={{color: "#AE3034"}} >Phone:<br/><h4 style={{color: "#000000"}} > 6476798830 </h4></h3><br></br>
              <h3 className='card-text' style={{color: "#AE3034"}}>Email:<br/><h4 style={{color: "#000000"}} > jose@gmail.com </h4></h3><br></br>
              <h3 className='card-text'style={{color: "#AE3034"}} >Company:<br/><h4 style={{color: "#000000"}} > Seeda </h4></h3>
              <a className='btn btn-primary' href='/Chat' style={{ margin:"5px"}}>Enter Chat</a>
            
            
          </div>

        </div>
    )
}

export default Card;