import React, {Component} from 'react';
import MapView from "./MapView";
import Card from "./Card";
import seeRemImg from './Images/seeRemImg.jpg'


const View = props => {
    return(
        <div style= {{width: '90vw', height: '90vh'}} >
            <div className='row' >
                <div className='col-sm-8' style= {{  margin: '40px'}}>
                    <MapView/>
                </div>
                <div className='col-sm-2' >
                    <Card/>
                </div>
                
            </div>
          
        </div>
    )
}

export default View;