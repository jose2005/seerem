import React from'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Chat from './Components/Chat'
import View from './Components/View'
import Login from './Components/Login'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/View">
          <View />
        </Route>
        <Route path="/Chat">
          <Chat />
        </Route> 
        <Route path="/">
          <Login />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

